# Implementační dokumentace modulu *Rush Hour Aggregator*

## Záměr

Modul agreguje dopravní data pro projekt Praha zasekaná.

## Vstupní data

Vstupem jsou 4 datové sady již existující v datové platformě:

    - SDDŘ (TSK) - zdroj public.tsk_std_measurements, public.tsk_std
    - FCD - public.fcd_traff_params_part
    - NDIC - public.ndic_traffic_info
    - Waze - public.wazeccp_jams

### Data aktivně stahujeme

Data se aktivně agregují pomocí cronu každých 5 minut. Kvůli možnému zpoždění obdržení některých dat je nastaven překryv a dohromady agreguje se posledních 10 minut. Existující položky jsou aktualizovaný na nový stav. Pro cron je nastavena priorita 1, pro VIP odbavení oproti historické agregaci viz níže.

Příklad nastavení cronu:
```json
  {
    "cronTime": "0 */5 * * * *",
    "exchange": "dataplatform",
    "message": "",
    "routingKey": "cron.dataplatform.rush-hour-aggregation.aggregateData",
    "options": {
      "priority": 1
    }
  }
```


Zároveň je také možné spustit historickou agregaci, ať už pro doplnění historických dat nebo v případech jejich změny. Tato agregace spouští manuálně vložením message pro rabbit mq viz readme dokument modulu.

## Zpracování dat / transformace


Pro účely agregace bylo vytvořeno 5 sql skriptů, které jsou samostatne uloženy ve složce ./templates/sqlQueries. V každém skriptu jsou použity parametry *from* a *to* a dle typu data jsou uvozeny \$ nebo :. Obě verze jsou rozpoznatelné a spustitelné v DBeaver, ale pro kód **nejsou zaměnitelné**. V případě : kód resp. knihovna sequelize se stará o doplnění proměných a v případě \$ se o to stará databázová část. Pro jednoduší sql skripty Waze, Fcd je použit \$. Pro složitější NDIC, SSDŘ se používá :.

Po nahraní dat do databáze se ještě "concurrently" obnoví nasledující materializované pohledy:

    - fcd_events_snapshots_latest
    - ndic_events_full_snapshots_latest
    - tsk_std_last_30min_snapshots_latest
    - waze_jams_snapshots_latest
    - waze_jams_snapshots_latest

Interní RabbitMQ fronty jsou popsány v [AsyncAPI](./asyncapi.yaml).

### *AggregationWorker* - Metoda *aggregateData*:

Podporuje několik scénářů zpracovaní podle obdržené zprávy:

1) Prázdná zpráva
Je obvykle generována cronem a spustí pro každý z typů agregace zpracování za posledních X minut dle nastavení workeru. V současnosti je tato hodnota nastavena na 10 minut.

2) Validní json zpráva
Validní json zpráva obsahuje údaje od, do a typ dat (v angličtině). Pro příklad zprávy viz readme modulu. Následně je zkontrolovano zda-li období zpracování není příliš dlouhé. Pro každý typ vstupních dat je toto období určeno jinak, dle množství generovaných dat a náročnosti zpracování:

    | data | max období |
    | --- | --- |
    | FCD | 30 minut |
    | NDIC | 2 hodiny |
    | SDDŘ | 2 hodiny |
    | Waze | 6 hodin |

Pokud je období dlouhé tak se rozdělí na subtasky, které se uloží do fronty ke zpracování.
V opačném případě se spustí agregační sql skript pro vybraný typ dat a výsledek se uloží do vybrané tabulky v db schématu praha_dopravni.

### *AggregationWorker* - Metoda *refreshHistoricViews*:

Je spuštěna cronem jednou za 24h s prázdnou zprávou a obnovuje "concurrently" historické pohledy, které připravují časová rozpětí pro již připravené agregace.

Seznam pohledů:
  - fcd_events_snapshots_history
  - ndic_events_full_snapshots_history
  - tsk_std_last_30min_snapshots_history
  - waze_jams_snapshots_history
  - waze_jams_snapshots_history


### Obecné

- typ databáze
  - PSQL
- datábázové schéma
  - praha_dopravni, public
- migrační skripty
  - ano, součastí modulu
- retence dat
  - n/a
