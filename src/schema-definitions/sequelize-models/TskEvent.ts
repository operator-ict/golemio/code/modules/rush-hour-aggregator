import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { Geometry } from "geojson";
import ITskEvent from "./interfaces/ITskEvent";

export default class TskEvent extends Model<TskEvent> implements ITskEvent {
    declare measured_at: number;
    declare geom: Geometry;
    declare sum: number | null;
    declare properties: object;

    public static attributeModel: ModelAttributes<TskEvent> = {
        measured_at: { type: DataTypes.BIGINT, primaryKey: true },
        geom: { type: DataTypes.GEOMETRY, primaryKey: true },
        sum: { type: DataTypes.INTEGER },
        properties: { type: DataTypes.JSON },
    };

    public static arrayJsonSchema: JSONSchemaType<ITskEvent[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                measured_at: { type: "number" },
                geom: { $ref: "#/definitions/geometry" },
                sum: { type: ["number", "null"] },
                properties: { type: "object" },
            },
            required: ["measured_at", "geom"],
        },
        definitions: {
            // @ts-expect-error since it is referenced definition from other file ts doesnt like it.
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
