import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";

export default class PrahaDopravniInfo {
    public static SCHEMA = "praha_dopravni";
    public static TABLE_NAMES = {
        [AggregationTaskType.WAZEJ]: "waze_jams",
        [AggregationTaskType.WAZER]: "waze_reconstructions",
        [AggregationTaskType.NDIC]: "ndic_events_full",
        [AggregationTaskType.SDDR]: "tsk_std_last_30min",
        [AggregationTaskType.FCD]: "fcd_events",
    };
}
