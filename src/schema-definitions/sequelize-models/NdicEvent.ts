import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { Geometry } from "geojson";
import INdicEvent from "./interfaces/INdicEvent";

export default class NdicEvent extends Model<NdicEvent> implements INdicEvent {
    declare situation_id: string;
    declare situation_record_type: string;
    declare situation_version_time: string;
    declare measured_at: number;
    declare geom_origin: Geometry;
    declare geom_centroid: Geometry;
    declare geom_startpoint: Geometry;
    declare geom_endpoint: Geometry;
    declare situation_urgency: string;
    declare geom_symbol_iconimage: string;
    declare geom_line_pattern: string;
    declare properties: object;

    public static attributeModel: ModelAttributes<NdicEvent> = {
        situation_id: { type: DataTypes.STRING, primaryKey: true },
        situation_record_type: { type: DataTypes.STRING, primaryKey: true },
        situation_version_time: { type: DataTypes.STRING, primaryKey: true },
        measured_at: { type: DataTypes.BIGINT, primaryKey: true },
        geom_origin: { type: DataTypes.GEOMETRY },
        geom_centroid: { type: DataTypes.GEOMETRY },
        geom_startpoint: { type: DataTypes.GEOMETRY },
        geom_endpoint: { type: DataTypes.GEOMETRY },
        situation_urgency: { type: DataTypes.STRING },
        geom_symbol_iconimage: { type: DataTypes.STRING },
        geom_line_pattern: { type: DataTypes.STRING },
        properties: { type: DataTypes.JSON },
    };

    public static arrayJsonSchema: JSONSchemaType<INdicEvent[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                measured_at: { type: "number" },
                geom_origin: { $ref: "#/definitions/geometry" },
                geom_centroid: { $ref: "#/definitions/geometry" },
                geom_startpoint: { $ref: "#/definitions/geometry" },
                geom_endpoint: { $ref: "#/definitions/geometry" },
                situation_urgency: { type: "string" },
                geom_symbol_iconimage: { type: "string" },
                geom_line_pattern: { type: "string" },
                properties: { type: "object" },
            },
            required: ["measured_at", "geom_origin"],
        },
        definitions: {
            // @ts-expect-error since it is referenced definition from other file ts doesnt like it.
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
