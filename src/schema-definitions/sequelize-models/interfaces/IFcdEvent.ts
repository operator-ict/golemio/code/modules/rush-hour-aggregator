import { Geometry } from "geojson";

export default interface IFcdEvent {
    source_identification: string;
    measured_at: number;
    oriented_route: Geometry;
    traffic_level: string;
    properties: object;
}
