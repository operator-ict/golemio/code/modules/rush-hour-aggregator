import { Geometry } from "geojson";

export default interface IWazeJams {
    id: string;
    measured_at: number;
    geom_origin: Geometry;
    geom_startpoint: Geometry;
    traffic_level: string;
    properties: object;
}
