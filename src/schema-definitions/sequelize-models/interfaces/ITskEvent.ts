import { Geometry } from "geojson";

export default interface ITskEvent {
    measured_at: number;
    geom: Geometry;
    sum: number | null;
    properties: object;
}
