export enum AggregationTaskType {
    SDDR = "SDDR",
    FCD = "FCD",
    NDIC = "NDIC",
    WAZER = "WAZER",
    WAZEJ = "WAZEJ",
}
