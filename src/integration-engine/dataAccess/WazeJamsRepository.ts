import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import PrahaDopravniInfo from "#sch/sequelize-models/PrahaDopravniInfo";
import WazeJams from "#sch/sequelize-models/WazeJams";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import EventsRepository from "./AbstractEventsRepository";

export default class WazeJamsRepository extends EventsRepository<WazeJams> {
    protected materializedViewNameLatest = "waze_jams_snapshots_latest";
    protected materializedViewNameHistoric = "waze_jams_snapshots_history";

    constructor() {
        super(
            "WazeJams",
            {
                outputSequelizeAttributes: WazeJams.attributeModel,
                pgTableName: PrahaDopravniInfo.TABLE_NAMES[AggregationTaskType.WAZEJ],
                pgSchema: PrahaDopravniInfo.SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("WazeJams", WazeJams.arrayJsonSchema, true)
        );
    }

    public saveData = async (data: WazeJams[], updateOnDuplicate: boolean) => {
        await this["sequelizeModel"].bulkCreate<WazeJams>(data, {
            ignoreDuplicates: !updateOnDuplicate,
            returning: false,
            updateOnDuplicate: updateOnDuplicate
                ? ["measured_at", "geom_origin", "geom_startpoint", "traffic_level", "properties"]
                : undefined,
        });
    };
}
