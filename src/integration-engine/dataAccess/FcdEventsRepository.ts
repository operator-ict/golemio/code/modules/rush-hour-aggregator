import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import FcdEvent from "#sch/sequelize-models/FcdEvents";
import PrahaDopravniInfo from "#sch/sequelize-models/PrahaDopravniInfo";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import EventsRepository from "./AbstractEventsRepository";

export default class FcdEventsRepository extends EventsRepository<FcdEvent> {
    protected materializedViewNameLatest = "fcd_events_snapshots_latest";
    protected materializedViewNameHistoric = "fcd_events_snapshots_history";

    constructor() {
        super(
            "FcdEventsRepository",
            {
                outputSequelizeAttributes: FcdEvent.attributeModel,
                pgTableName: PrahaDopravniInfo.TABLE_NAMES[AggregationTaskType.FCD],
                pgSchema: PrahaDopravniInfo.SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("FcdEventsRepository", FcdEvent.arrayJsonSchema, true)
        );
    }

    public saveData = async (data: FcdEvent[], updateOnDuplicate: boolean) => {
        await this["sequelizeModel"].bulkCreate<FcdEvent>(data, {
            ignoreDuplicates: !updateOnDuplicate,
            returning: false,
            updateOnDuplicate: updateOnDuplicate ? ["oriented_route", "traffic_level", "properties"] : undefined,
        });
    };
}
