import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { BindOrReplacements, QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { FatalError } from "@golemio/core/dist/shared/golemio-errors";
import fs from "fs";
import path from "path";

export default class AggregationRepository {
    private static instance: AggregationRepository;
    private sqlQueriesFileNames: Map<AggregationTaskType, string>;
    private sqlQueries: Map<AggregationTaskType, string>;
    private relativeModuleRootPath: string = "../../../"; // <= src/#ie/dataAccess
    private sqlQueriesFolder: string = "templates/sqlQueries/";

    public static getInstance() {
        if (!AggregationRepository.instance) {
            AggregationRepository.instance = new AggregationRepository();
        }

        return AggregationRepository.instance;
    }

    private constructor() {
        this.sqlQueriesFileNames = new Map<AggregationTaskType, string>([
            [AggregationTaskType.FCD, "FcdEvents.sql"],
            [AggregationTaskType.NDIC, "NdicEventsFull.sql"],
            [AggregationTaskType.SDDR, "TskEvents.sql"],
            [AggregationTaskType.WAZEJ, "WazeJamsAggregation.sql"],
            [AggregationTaskType.WAZER, "WazeReconstructionsAggregation.sql"],
        ]);
        this.sqlQueries = new Map<AggregationTaskType, string>();
    }

    public aggregationQuery = async <T extends object>(
        type: AggregationTaskType,
        bindings?: BindOrReplacements,
        replacements?: BindOrReplacements
    ): Promise<T[]> => {
        return await PostgresConnector.getConnection().query<T>(this.loadSqlQuery(type), {
            type: QueryTypes.SELECT,
            raw: true,
            bind: bindings,
            replacements: replacements,
        });
    };

    protected loadSqlQuery = (type: AggregationTaskType): string => {
        if (!this.sqlQueries.get(type)) {
            const queryFileName = this.sqlQueriesFileNames.get(type);
            if (!queryFileName) throw new FatalError(`Couldn't find sql query for type: ${type}`, this.constructor.name);

            const pathToQuery = path.resolve(__dirname, this.relativeModuleRootPath, this.sqlQueriesFolder, queryFileName);
            this.sqlQueries.set(type, fs.readFileSync(pathToQuery).toString());
        }

        return this.sqlQueries.get(type)!;
    };
}
