import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import PrahaDopravniInfo from "#sch/sequelize-models/PrahaDopravniInfo";
import WazeReconstructions from "#sch/sequelize-models/WazeReconstructions";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import EventsRepository from "./AbstractEventsRepository";

export default class WazeReconstructionsRepository extends EventsRepository<WazeReconstructions> {
    protected materializedViewNameLatest = "waze_reconstructions_snapshots_latest";
    protected materializedViewNameHistoric = "waze_reconstructions_snapshots_history";

    constructor() {
        super(
            "WazeReconstructions",
            {
                outputSequelizeAttributes: WazeReconstructions.attributeModel,
                pgTableName: PrahaDopravniInfo.TABLE_NAMES[AggregationTaskType.WAZER],
                pgSchema: PrahaDopravniInfo.SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("WazeReconstructions", WazeReconstructions.arrayJsonSchema, true)
        );
    }

    public saveData = async (data: WazeReconstructions[], updateOnDuplicate: boolean) => {
        await this["sequelizeModel"].bulkCreate<WazeReconstructions>(data, {
            ignoreDuplicates: !updateOnDuplicate,
            returning: false,
            updateOnDuplicate: updateOnDuplicate ? ["measured_at", "geom_origin", "geom_startpoint", "properties"] : undefined,
        });
    };
}
