import PrahaDopravniInfo from "#sch/sequelize-models/PrahaDopravniInfo";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractBasicRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractBasicRepository";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";

export class DataRetentionRepository extends AbstractBasicRepository {
    public schema: string = PrahaDopravniInfo.SCHEMA;
    public tableName: string = "";

    constructor(connector: IDatabaseConnector, log: ILogger) {
        super(connector, log);
    }

    public async deleteOldData() {
        await this.connector.getConnection().query(`CALL ${this.schema}.data_retention()`, {
            plain: true,
        });
    }
}
