import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";

export default interface IAggregationTask {
    from?: Date;
    to?: Date;
    type?: AggregationTaskType;
}
