import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import FcdEventsAggregator from "./aggregators/FcdEventsAggregator";
import IAbstractAggregator from "./aggregators/interfaces/IAbstractAggregator";
import NdicEventsAggregator from "./aggregators/NdicEventsAggregator";
import TskEventsAggregator from "./aggregators/TskEventsAggregator";
import WazeJamsAggregator from "./aggregators/WazeJamsAggregator";
import WazeReconstructionsAggregator from "./aggregators/WazeReconstructionsAggregator";

export default class AggregationFactory {
    private products: Map<AggregationTaskType, IAbstractAggregator>;

    constructor() {
        this.products = new Map<AggregationTaskType, IAbstractAggregator>([
            [AggregationTaskType.FCD, new FcdEventsAggregator()],
            [AggregationTaskType.NDIC, new NdicEventsAggregator()],
            [AggregationTaskType.SDDR, new TskEventsAggregator()],
            [AggregationTaskType.WAZEJ, new WazeJamsAggregator()],
            [AggregationTaskType.WAZER, new WazeReconstructionsAggregator()],
        ]);
    }

    public get(type: AggregationTaskType): IAbstractAggregator {
        if (!this.products.has(type)) {
            throw new GeneralError(`Unknown aggregation task type: ${type}`, this.constructor.name);
        }

        return this.products.get(type)!;
    }
}
