import AggregationRepository from "#ie/dataAccess/AggregationRepository";
import FcdEventsRepository from "#ie/dataAccess/FcdEventsRepository";
import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import FcdEvent from "#sch/sequelize-models/FcdEvents";
import IFcdEvent from "#sch/sequelize-models/interfaces/IFcdEvent";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import AbstractAggregator from "./AbstractAggregator";

export default class FcdEventsAggregator extends AbstractAggregator<FcdEvent> {
    protected override maxAggregationDuration = { minutes: 30 };
    protected repository: FcdEventsRepository;

    constructor() {
        super();
        this.repository = new FcdEventsRepository();
    }

    protected aggregate = async (from: Date, to: Date): Promise<FcdEvent[]> => {
        const result = await AggregationRepository.getInstance().aggregationQuery<IFcdEvent>(AggregationTaskType.FCD, {
            from: DateTime.fromJSDate(from).toISO(),
            to: DateTime.fromJSDate(to).toISO(),
        });

        return this.mapToDto(result);
    };

    protected mapToDto(data: any[]): FcdEvent[] {
        return data.map((element) => {
            return {
                source_identification: element.source_identification,
                measured_at: Number.parseInt(element.measured_at),
                oriented_route: element.oriented_route,
                traffic_level: element.traffic_level,
                properties: element.properties,
            } as FcdEvent;
        });
    }
}
