import AggregationRepository from "#ie/dataAccess/AggregationRepository";
import WazeJamsRepository from "#ie/dataAccess/WazeJamsRepository";
import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import IWazeJams from "#sch/sequelize-models/interfaces/IWazeJams";
import WazeJams from "#sch/sequelize-models/WazeJams";
import AbstractAggregator from "./AbstractAggregator";

export default class WazeAggregator extends AbstractAggregator<WazeJams> {
    protected repository: WazeJamsRepository;

    constructor() {
        super();
        this.repository = new WazeJamsRepository();
    }

    protected aggregate = async (from: Date, to: Date): Promise<WazeJams[]> => {
        const result = await AggregationRepository.getInstance().aggregationQuery<IWazeJams>(AggregationTaskType.WAZEJ, {
            from,
            to,
        });

        return this.mapToDto(result);
    };

    protected mapToDto(data: any[]): WazeJams[] {
        return data.map((element) => {
            return {
                id: element.id,
                measured_at: Number.parseInt(element.measured_at),
                geom_origin: element.geom_origin,
                geom_startpoint: element.geom_startpoint,
                traffic_level: element.traffic_level,
                properties: element.properties,
            } as WazeJams;
        });
    }
}
