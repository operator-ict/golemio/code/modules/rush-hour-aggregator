import AggregationRepository from "#ie/dataAccess/AggregationRepository";
import NdicEventsRepository from "#ie/dataAccess/NdicEventRepository";
import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import INdicEvent from "#sch/sequelize-models/interfaces/INdicEvent";
import NdicEvent from "#sch/sequelize-models/NdicEvent";
import { DateTime, DurationLike } from "@golemio/core/dist/shared/luxon";
import AbstractAggregator from "./AbstractAggregator";

export default class NdicEventsAggregator extends AbstractAggregator<NdicEvent> {
    protected override maxAggregationDuration: DurationLike = { hours: 2 };
    protected repository: NdicEventsRepository;

    constructor() {
        super();
        this.repository = new NdicEventsRepository();
    }

    protected aggregate = async (from: Date, to: Date): Promise<NdicEvent[]> => {
        const result = await AggregationRepository.getInstance().aggregationQuery<INdicEvent>(
            AggregationTaskType.NDIC,
            undefined,
            {
                from: DateTime.fromJSDate(from).toISO(),
                to: DateTime.fromJSDate(to).toISO(),
            }
        );

        return this.mapToDto(result);
    };

    protected mapToDto(data: any[]): NdicEvent[] {
        return data.map((element) => {
            return {
                situation_id: element.situation_id,
                situation_record_type: element.situation_record_type,
                situation_version_time: element.situation_version_time,
                measured_at: Number.parseInt(element.measured_at),
                geom_origin: element.geom_origin,
                geom_centroid: element.geom_centroid,
                geom_startpoint: element.geom_startpoint,
                geom_endpoint: element.geom_endpoint,
                situation_urgency: element.situation_urgency,
                geom_symbol_iconimage: element.geom_symbol_iconimage,
                geom_line_pattern: element.geom_line_pattern,
                properties: element.properties,
            } as NdicEvent;
        });
    }
}
