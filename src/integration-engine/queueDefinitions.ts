import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AggregationWorker } from "./AggregationWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: AggregationWorker.name,
        queuePrefix: AggregationWorker.queuePrefix,
        queues: [
            {
                name: AggregationWorker.aggregationMethodName,
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                    maxPriority: 1,
                    arguments: { "x-queue-type": "classic" },
                },
                worker: AggregationWorker,
                workerMethod: AggregationWorker.aggregationMethodName,
            },
            {
                name: AggregationWorker.refreshHistoricViewsMethodName,
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: AggregationWorker,
                workerMethod: AggregationWorker.refreshHistoricViewsMethodName,
            },
        ],
    },
];

export { queueDefinitions };
