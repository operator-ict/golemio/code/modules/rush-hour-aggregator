import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { DateTime, DurationLike } from "@golemio/core/dist/shared/luxon";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Message } from "amqplib";
import AggregationTask from "./business/AggregationTask";

export default class WorkerHelper {
    public static parseMessage(message: Message): AggregationTask {
        const task = WorkerHelper.parseContent(message.content.toString());

        if (!task) return {} as AggregationTask;

        const type = (<any>AggregationTaskType)[task.type?.toUpperCase()];
        if (!type) throw new GeneralError(`Unknown aggregation task type ${JSON.stringify(task)}`, this.constructor.name);
        if (!task.from || !task.to)
            throw new GeneralError(`Unsupported aggregation task ${JSON.stringify(task)}`, this.constructor.name);

        const result = new AggregationTask(
            DateTime.fromISO(task.from).toJSDate(),
            DateTime.fromISO(task.to).toJSDate(),
            type,
            task.updateOnDuplicate ? task.updateOnDuplicate.toString().toLowerCase() == "true" : undefined,
            task.refreshView ? task.refreshView.toString().toLowerCase() == "true" : undefined
        );

        if (result.from! > result.to!)
            throw new GeneralError(`Unsupported task date range ${JSON.stringify(task)}`, this.constructor.name);

        return result;
    }

    public static generateIntervals = (
        from: DateTime,
        to: DateTime,
        maxDuration: DurationLike
    ): Array<{ start: Date; end: Date }> => {
        const breakpoints = [from];
        let tmpFrom = from.plus(maxDuration);

        while (tmpFrom.diff(to, "milliseconds").milliseconds < 0) {
            breakpoints.push(tmpFrom);
            tmpFrom = tmpFrom.plus(maxDuration);
        }

        if (tmpFrom.diff(to, "milliseconds").milliseconds >= 0) {
            breakpoints.push(to);
        }

        const intervalsOutput = [];
        for (let i = 0; i < breakpoints.length - 1; i++) {
            const current = breakpoints[i];
            const next = breakpoints[i + 1];
            intervalsOutput.push({ start: current.toJSDate(), end: next.toJSDate() });
        }

        return intervalsOutput;
    };

    public static generateSubTasksDefinitions = (
        from: DateTime,
        to: DateTime,
        maxDuration: DurationLike,
        taskType: AggregationTaskType,
        updateOnDuplicate: boolean,
        refreshView: boolean
    ): AggregationTask[] => {
        const result: AggregationTask[] = [];
        const intervals = WorkerHelper.generateIntervals(from, to, maxDuration);
        for (const item of intervals) {
            log.silly(`generating task from:${item.start.toISOString()}, to:${item.end.toISOString()}`);
            result.push(new AggregationTask(item.start, item.end, taskType, updateOnDuplicate, refreshView));
        }

        return result;
    };

    private static parseContent(messageContent: string) {
        try {
            return messageContent.length > 0 ? JSON.parse(messageContent) : undefined;
        } catch (err) {
            throw new GeneralError(`Unable to parse json from message: ${messageContent}`, this.constructor.name, err);
        }
    }
}
