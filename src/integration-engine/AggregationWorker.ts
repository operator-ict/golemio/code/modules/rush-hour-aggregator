import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import { BaseWorker, PostgresConnector, log } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime, Duration } from "@golemio/core/dist/shared/luxon";
import { Message } from "amqplib";
import WorkerHelper from "./WorkerHelper";
import AggregationTask from "./business/AggregationTask";
import { DataRetentionRepository } from "./dataAccess/DataRetentionRepository";
import AggregationFactory from "./service/AggregationFactory";

export class AggregationWorker extends BaseWorker {
    public static queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + "rush-hour-aggregation";
    public static aggregationMethodName = "aggregateData";
    public static refreshHistoricViewsMethodName = "refreshHistoricViews";
    private aggregationFactory: AggregationFactory;
    private refreshIntervalInMinutes: number = 10;
    private dataRetentionRepository: DataRetentionRepository;

    constructor() {
        super();
        this.aggregationFactory = new AggregationFactory();
        this.dataRetentionRepository = new DataRetentionRepository(PostgresConnector, log);
    }

    public aggregateData = async (message: Message) => {
        const task = WorkerHelper.parseMessage(message);
        log.debug(`Received new task: ${JSON.stringify(task)}`);
        if (this.isTaskEmpty(task)) {
            await this.handleEmptyTask();
        } else if (this.isDateRangeBig(task)) {
            await this.generateSubTasks(task);
        } else if (task.type && task.from && task.to) {
            await this.processTask(task.type, task.from, task.to, task.updateOnDuplicate, task.refreshView);
        } else {
            throw new GeneralError(`Unable to process task: ${JSON.stringify(task)}.`, this.constructor.name);
        }
    };

    public refreshHistoricViews = async () => {
        await this.dataRetentionRepository.deleteOldData();

        for (const type in AggregationTaskType) {
            try {
                const aggregator = this.aggregationFactory.get(type.toUpperCase() as AggregationTaskType);
                await aggregator.refreshHistoricMaterialView();
            } catch (err) {
                log.error(`Unable to refresh historic material view for: ${type}. Error stack: ${err.stack}`);
            }
        }
    };

    private isTaskEmpty(task: AggregationTask) {
        return !task.from && !task.to && !task.type;
    }

    private isDateRangeBig(task: AggregationTask) {
        if (task.from && task.to && task.type) {
            const maxDuration = Duration.fromDurationLike(this.aggregationFactory.get(task.type).getMaxDuration());
            const difference = DateTime.fromJSDate(task.to).diff(DateTime.fromJSDate(task.from), "hours");

            return difference && difference > maxDuration;
        }

        return false;
    }

    private handleEmptyTask = async (): Promise<void> => {
        const to = new Date();
        const from = DateTime.fromJSDate(to).minus({ minutes: this.refreshIntervalInMinutes }).toJSDate();
        for (const type in AggregationTaskType) {
            await this.processTask(type, from, to, true, true);
        }
    };

    private processTask = async (
        type: string,
        from: Date,
        to: Date,
        updateOnDuplicate: boolean,
        refreshView: boolean
    ): Promise<void> => {
        try {
            const aggregator = this.aggregationFactory.get(type.toUpperCase() as AggregationTaskType);
            await aggregator.aggregateAndSave(from, to, updateOnDuplicate, refreshView);
        } catch (err) {
            log.error(`Unable to process task: ${type} from: ${from} to: ${to}. (${err.toString()}) Error stack: ${err.stack}`);
        }
    };

    private generateSubTasks = async (task: AggregationTask): Promise<void> => {
        const subtasks = WorkerHelper.generateSubTasksDefinitions(
            DateTime.fromJSDate(task.from!),
            DateTime.fromJSDate(task.to!),
            this.aggregationFactory.get(task.type!).getMaxDuration(),
            task.type!,
            task.updateOnDuplicate,
            task.refreshView
        );

        for (const subtask of subtasks) {
            await this.sendMessageToExchange(
                "workers." + AggregationWorker.queuePrefix + "." + AggregationWorker.aggregationMethodName,
                JSON.stringify(subtask)
            );
        }
    };
}
