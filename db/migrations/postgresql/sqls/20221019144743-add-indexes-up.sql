CREATE INDEX fcd_events_measured_at_idx ON fcd_events(measured_at int8_ops);

CREATE INDEX ndic_events_full_measured_at_pk ON ndic_events_full(measured_at int8_ops);
CREATE INDEX ndic_events_full_situation_record_type_idx ON ndic_events_full(situation_record_type text_ops);

CREATE INDEX tsk_std_last_30min_measured_at_idx ON tsk_std_last_30min(measured_at int8_ops);

CREATE INDEX waze_jams_measured_at_idx ON waze_jams(measured_at int8_ops);

CREATE INDEX waze_reconstructions_measured_at_idx ON waze_reconstructions(measured_at int8_ops);