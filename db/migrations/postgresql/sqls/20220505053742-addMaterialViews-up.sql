--
-- WAZE
--

CREATE MATERIALIZED VIEW waze_reconstructions_snapshots_history AS  WITH config AS (
         SELECT 120000 AS max_period,
            date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000::double precision AS max_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT waze_reconstructions.measured_at
   FROM waze_reconstructions  WHERE waze_reconstructions.measured_at::double precision < (( SELECT config.max_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX waze_reconstructions_snapshots_history_start_measured_at_idx ON waze_reconstructions_snapshots_history(start_measured_at int8_ops);


------
------

  CREATE MATERIALIZED VIEW waze_reconstructions_snapshots_latest AS  WITH config AS (
         SELECT 120000 AS max_period,
            date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000::double precision AS min_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT waze_reconstructions.measured_at
   FROM waze_reconstructions  WHERE waze_reconstructions.measured_at::double precision >= (( SELECT config.min_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------
CREATE UNIQUE INDEX waze_reconstructions_snapshots_latest_start_measured_at_idx ON waze_reconstructions_snapshots_latest(start_measured_at int8_ops);






-- Table Definition ----------------------------------------------

CREATE MATERIALIZED VIEW waze_jams_snapshots_history AS  WITH config AS (
         SELECT 120000 AS max_period,
            date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000::double precision AS max_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT waze_jams.measured_at
   FROM waze_jams  WHERE waze_jams.measured_at::double precision < (( SELECT config.max_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX waze_jams_snapshots_history_start_measured_at_idx ON waze_jams_snapshots_history(start_measured_at int8_ops);


-- Table Definition ----------------------------------------------

CREATE MATERIALIZED VIEW waze_jams_snapshots_latest AS  WITH config AS (
         SELECT 120000 AS max_period,
            date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000::double precision AS min_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT waze_jams.measured_at
   FROM waze_jams WHERE waze_jams.measured_at::double precision >= (( SELECT config.min_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX waze_jams_snapshots_latest_start_measured_at_idx ON waze_jams_snapshots_latest(start_measured_at int8_ops);



---
---
---  TSK STD
---
---

-- Table Definition ----------------------------------------------

CREATE MATERIALIZED VIEW tsk_std_last_30min_snapshots_history AS  WITH config AS (
         SELECT 300000 AS max_period,
            date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000::double precision AS max_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT tsk_std_last_30min.measured_at
   FROM tsk_std_last_30min  WHERE tsk_std_last_30min.measured_at::double precision < (( SELECT config.max_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX tsk_std_last_30min_snapshots_history_start_measured_at_idx ON tsk_std_last_30min_snapshots_history(start_measured_at int8_ops);

-- Table Definition ----------------------------------------------

CREATE MATERIALIZED VIEW tsk_std_last_30min_snapshots_latest AS  WITH config AS (
         SELECT 300000 AS max_period,
            date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000::double precision AS min_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT tsk_std_last_30min.measured_at
   FROM tsk_std_last_30min  WHERE tsk_std_last_30min.measured_at::double precision >= (( SELECT config.min_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX tsk_std_last_30min_snapshots_latest_start_measured_at_idx ON tsk_std_last_30min_snapshots_latest(start_measured_at int8_ops);





--
--
-- NDIC
--
--

-- Table Definition ----------------------------------------------

CREATE MATERIALIZED VIEW ndic_events_full_snapshots_history AS  WITH config AS (
         SELECT 1800000 AS max_period,
            date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000::double precision AS max_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT ndic_events_full.measured_at
   FROM ndic_events_full  WHERE ndic_events_full.measured_at::double precision < (( SELECT config.max_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX ndic_events_full_snapshots_history_start_measured_at_idx ON ndic_events_full_snapshots_history(start_measured_at int8_ops);

-- Table Definition ----------------------------------------------

CREATE MATERIALIZED VIEW ndic_events_full_snapshots_latest AS  WITH config AS (
         SELECT 1800000 AS max_period,
            date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000::double precision AS min_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT ndic_events_full.measured_at
   FROM ndic_events_full WHERE ndic_events_full.measured_at::double precision >= (( SELECT config.min_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX ndic_events_full_snapshots_latest_start_measured_at_idx ON ndic_events_full_snapshots_latest(start_measured_at int8_ops);




--
--
-- FCD
--
--

-- Table Definition ----------------------------------------------

CREATE MATERIALIZED VIEW fcd_events_snapshots_history AS  WITH config AS (
         SELECT 60000 AS max_period,
            (date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000)::int8 AS max_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT fcd_events.measured_at
   FROM fcd_events WHERE fcd_events.measured_at < (( SELECT config.max_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX fcd_events_snapshots_history_start_measured_at_idx ON fcd_events_snapshots_history(start_measured_at int8_ops);


-- Table Definition ----------------------------------------------

CREATE MATERIALIZED VIEW fcd_events_snapshots_latest AS  WITH config AS (
         SELECT 60000 AS max_period,
            (date_part('epoch'::text, date_trunc('day',now(), 'Europe/Prague')) * 1000)::int8 AS min_measured_at
        )
 SELECT s.start_measured_at,
    s.end_measured_at,
    to_timestamp((s.start_measured_at / 1000)::double precision) AS start_ts,
    to_timestamp((s.end_measured_at / 1000)::double precision) AS end_ts
   FROM ( SELECT s_1.measured_at AS start_measured_at,
                CASE
                    WHEN s_1.is_end IS TRUE THEN s_1.measured_at
                    ELSE lead(s_1.measured_at) OVER (ORDER BY s_1.measured_at)
                END + (( SELECT config.max_period
                   FROM config)) AS end_measured_at,
            s_1.is_start,
            s_1.is_end
           FROM ( SELECT s_2.measured_at,
                    s_2.prev_measured_at,
                    s_2.next_measured_at,
                    s_2."?column?",
                    s_2."?column?_1" AS "?column?",
                    s_2.is_start,
                    s_2.is_end
                   FROM ( SELECT s_3.measured_at,
                            s_3.prev_measured_at,
                            s_3.next_measured_at,
                            s_3.measured_at - s_3.prev_measured_at,
                            s_3.measured_at - s_3.next_measured_at,
                                CASE
                                    WHEN s_3.prev_measured_at < (s_3.measured_at - (( SELECT config.max_period
                                       FROM config))) OR s_3.prev_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_start,
                                CASE
                                    WHEN s_3.next_measured_at > (s_3.measured_at + (( SELECT config.max_period
                                       FROM config))) OR s_3.next_measured_at IS NULL THEN true
                                    ELSE false
                                END AS is_end
                           FROM ( SELECT s_4.measured_at,
                                    lag(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS prev_measured_at,
                                    lead(s_4.measured_at) OVER (ORDER BY s_4.measured_at) AS next_measured_at
                                   FROM ( SELECT DISTINCT fcd_events.measured_at
   FROM fcd_events WHERE fcd_events.measured_at >= (( SELECT config.min_measured_at
           FROM config))) s_4) s_3) s_2(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)
                  WHERE s_2.is_start IS TRUE OR s_2.is_end IS TRUE) s_1(measured_at, prev_measured_at, next_measured_at, "?column?", "?column?_1", is_start, is_end)) s
  WHERE s.is_start IS TRUE;
-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX fcd_events_snapshots_latest_start_measured_at_idx ON fcd_events_snapshots_latest(start_measured_at int8_ops);
