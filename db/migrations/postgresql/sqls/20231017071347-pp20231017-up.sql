CREATE TABLE fcd_geos_praha_dopravni (
	source_identification varchar(100) NOT NULL,
	oriented_route public.geometry NULL,
	"version" varchar(50) NULL,
	CONSTRAINT fcd_geos_90_praha_dopravni PRIMARY KEY (source_identification)
);

CREATE OR REPLACE FUNCTION fcd_get_route(p_source_id character varying,p_version character varying)
 RETURNS geometry
 LANGUAGE plpgsql
AS $function$
	declare 
		x_id_start integer;
		x_id_end integer;
		x_id_next integer;
		x_point geometry;
		x_lat real;
		x_long real;
		x_lat_next real;
		x_long_next real;
		x_final geometry;
		x_geom_txt varchar(5000);
	begin
		-- na vstupu očekávám takový řetězec - TS01243T01245
		-- kde první číslo se cíl, druhé start
		-- pokud na vstupu něco jiného vracím null a končím.
--		if p_source_id not like 'TS_____T_____' then
		if p_source_id not like 'TS%T%' then
			return null::geometry;
		end if;

		
	--	x_id_start = substring(p_source_id,9,5)::integer;
	--	x_id_end = substring(p_source_id,3,5)::integer;
		x_id_start = substring(p_source_id,position('T' in substring(p_source_id,2,50))+2,50);
		x_id_end = substring(p_source_id,3,position('T' in substring(p_source_id,2,50))-2);

	
		-- positivní větev
		
		select wgs84_x, wgs84_y,pos_off  
		into x_lat,x_long,x_id_next
		-- from public.fcd_lt_points
		from public.rsd_tmc_points  
		where lcd = x_id_start
		and version_id = p_version;

		select wgs84_x, wgs84_y,pos_off  
		into x_lat_next,x_long_next
		from public.rsd_tmc_points
		where lcd = x_id_next
		and version_id = p_version;
		
		x_geom_txt = 'linestring ('||x_lat::varchar(150)||' '||x_long::varchar(150)||','||x_lat_next::varchar(150) || ' ' || x_long_next::varchar(150)||')';
		x_final = ST_GEOMETRYFROMTEXT(x_geom_txt,4326);
	
		while x_id_next != x_id_end loop
			-- přidání dalšího bodu
			select wgs84_x, wgs84_y,pos_off  
			into x_lat_next,x_long_next,x_id_next
			from public.rsd_tmc_points
			where lcd = x_id_next
			and version_id = p_version;
		
			x_final = ST_AddPoint(x_final, ST_SetSRID(ST_MakePoint(x_lat_next, x_long_next),4326));
		end loop;
	
		if x_id_next = x_id_end then
			return x_final;
		end if;
	
-- negativní větev
		select wgs84_x, wgs84_y,neg_off  
		into x_lat,x_long,x_id_next
		from public.rsd_tmc_points
		where lcd = x_id_start
		and version_id = p_version;		

		select wgs84_x, wgs84_y,neg_off  
		into x_lat_next,x_long_next
		from public.rsd_tmc_points
		where lcd = x_id_next
		and version_id = p_version;
		
		x_geom_txt = 'linestring ('||x_lat::varchar(150)||' '||x_long::varchar(150)||','||x_lat_next::varchar(150) || ' ' || x_long_next::varchar(150)||')';
--		x_final = x_geom_txt::geometry;
		x_final = ST_GEOMETRYFROMTEXT(x_geom_txt,4326);
	
		while x_id_next != x_id_end loop
			-- přidání dalšího bodu
			select wgs84_x, wgs84_y,neg_off  
			into x_lat_next,x_long_next,x_id_next
			from public.rsd_tmc_points
			where lcd = x_id_next
			and version_id = p_version;
		
--			x_final = ST_AddPoint(x_final, ST_MakePoint(x_lat_next, x_long_next));
			x_final = ST_AddPoint(x_final, ST_SetSRID(ST_MakePoint(x_lat_next, x_long_next),4326));
		end loop;
	
		if x_id_next = x_id_end then
			return x_final;
		end if;
	
		return null::geometry;
	END;
$function$
;

CREATE OR REPLACE FUNCTION fcd_geos_fill(p_version character varying)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
	declare 
		x_id_start integer;
		x_id_end integer;
		x_id_next integer;
		x_point geometry;
		x_lat real;
		x_long real;
		x_lat_next real;
		x_long_next real;
		x_final geometry;
		x_line geometry;
		x_geom_txt varchar(5000);
		x_x varchar(5000);
		query_dataset record;
	begin
		FOR  query_dataset IN
	        select lcd
	        from public.rsd_tmc_roads rtp  
			where version_id = 'v10.1'
		loop
			insert into praha_dopravni.fcd_geos_praha_dopravni
			select --rtp.lcd,rtp2.lcd,
			'TS'||repeat('0',5-length(rtp.lcd::varchar))||rtp.lcd||'T'||repeat('0',5-length(rtp2.lcd::varchar))||rtp2.lcd, 
			 praha_dopravni.fcd_get_route('TS'||rtp.lcd||'T'||rtp2.lcd,p_version ),p_version
			from public.rsd_tmc_points rtp,public.rsd_tmc_points rtp2 
			where rtp.roa_lcd = query_dataset.lcd and rtp2.roa_lcd = query_dataset.lcd and rtp.version_id = p_version and rtp2.version_id = p_version
			and rtp.lcd != rtp2.lcd;
		end loop;
		-- return null;
	END;
$function$
;