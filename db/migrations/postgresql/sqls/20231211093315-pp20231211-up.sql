CREATE TABLE tmc_ltcze_roads_wgs84 (
	geom public.geometry(multilinestring, 4326) NULL,
	cid int8 NULL,
	tabcd int8 NULL,
	lcd int8 NOT NULL,
	"class" varchar(254) NULL,
	tcd int8 NULL,
	stcd int8 NULL,
	roadnumber varchar(254) NULL,
	roadname varchar(254) NULL,
	firstname varchar(254) NULL,
	secondname varchar(254) NULL,
	area_ref int8 NULL,
	area_name varchar(254) NULL,
	version_id varchar(20) NULL,
	CONSTRAINT tmc_ltcze_roads_wgs84_pkey PRIMARY KEY (lcd,version_id)
);
COMMENT ON TABLE tmc_ltcze_roads_wgs84 IS 'Data for this table are imported manually. Purpose of this table is to show static TMC road net in app Praha Dopravni.';

