<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>

<h1>@golemio/rush-hour-aggregation</h1>

<p>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/rush-hour-aggregation/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/rush-hour-aggregation/badges/master/pipeline.svg" alt="pipeline">
    </a>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/rush-hour-aggregation/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/rush-hour-aggregation/badges/master/coverage.svg" alt="coverage">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/npm/l/@golemio/rush-hour-aggregation" alt="license">
    </a>
</p>

<p>
    <a href="#installation">Installation</a> · <a href="./docs">Documentation</a> · <a href="https://operator-ict.gitlab.io/golemio/code/modules/rush-hour-aggregation">TypeDoc</a>
</p>
</div>

This module is intended for use with Golemio services. Refer [here](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/development/README.md) for further information on usage, local development and more.

## Installation

The APIs may be unstable. Therefore, we recommend to install this module as an exact version.

```bash
# Latest version
npm install --save-exact @golemio/rush-hour-aggregation@latest

# Development version
npm install --save-exact @golemio/rush-hour-aggregation@dev
```

## Description

This module aggregates traffic data for Praha dopravní project. It takes different traffic dataset avaiable in a golemio dataplatform and creates time series data for traffic jams, incidents or reconstructions.

[Rabin Praha dopravní](https://praha-dopravni-backend.rabin.golemio.cz/app)
[Backend repo Praha dopravní](https://gitlab.com/operator-ict/golemio/code/praha-dopravni)

## Datasources

-   Waze
-   SDDŘ
-   FCD
-   NDIC

## Current setup

Every 5 minutes worker starts and for each of the datasource aggregation is run and timeseries data are updated.

## Aggregation of older data

It is possible to insert or update historic data by creation of manual message in RabbitMQ.
E.g. to insert data for Fcd for first month in 2022 use following message:

```json
{
    "from": "2020-01-01T00:00:00Z",
    "to": "2020-01-31T23:59:59Z",
    "type": "fcd"
}
```

As of version 1.0.5 for FCD put maximum one month in one message. If whole year needs to be updated send 12 messages. For NDIC and SDDR use max 3 months and for waze 6 months.

## Message building blocks

Message has to be in json format and supports following fields:

_from_ - Required date time setting when the aggregation has to start. It is formatted in ISO 8601 preferably UTC format with Z at the end

_to_ - Required date time setting when the aggregation has is ending. It is formatted in ISO 8601 preferably UTC format with Z at the end

_type_ - Required setting which aggregation should be run. Possible values: WAZEJ, WAZER, SDDR, FCD, NDIC. (Case insensitive)

_updateOnDuplicate_ - Optional setting for situation when upgrade of already aggregated data is required. Possible values "true", "false". Default value is false. (Note: automatic 5 minute updates have the setting to true)

_refreshView_ - Optional setting to refresh latest materialized view. Possible values "true", "false". Default value is false. (Note: automatic 5 minute updates have the setting to true)

To initiate automatic 5 minute aggregation manually it is possible to send empty message.
