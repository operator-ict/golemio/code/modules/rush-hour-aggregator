select
	agg.measured_at,
	agg.geom,
	agg.sum,
	json_build_object(
			'sum', agg.sum,
	        'street', agg.street,
	        'related_details', agg.related_details
	) as properties
from (
	with kalendar as (
		select
			a.a
		from
			generate_series(
                DATE_TRUNC('minute',(timestamp with time zone :from)) - make_interval(mins => mod(EXTRACT(mins FROM timestamp with time zone :from)::integer, 5)::integer),
                DATE_TRUNC('minute',(timestamp with time zone :to)),
                '00:05:00'::interval
            ) a(a)
	),
	ciselnik as (
		select distinct
            tsk_std.latitude,
			tsk_std.longitude,
			((archive.tsk_std.street::text || '('::text) || archive.tsk_std.segment::text) || ')'::text as lokace,
			st_setsrid(st_makepoint(archive.tsk_std.longitude, archive.tsk_std.latitude), 4326) as geom
		from
			archive.tsk_std
		where
			tsk_std.latitude is not null and archive.tsk_std.longitude is not null
	),
	sdata as (
		select
			kalendar_1.a as time_5min,
			tsk_std.latitude,
			tsk_std.longitude,
			mes.detector_id,
			mes.class_id,
			mes.measurement_type,
			mes.value
		from
			kalendar kalendar_1
		cross join archive.tsk_std
		left join (
            select
                measured_from,
                measured_to,
                detector_id,
                class_id,
                measurement_type,
                value
            from archive.tsk_std_measurements
            where
                measured_from >= (date_part('epoch'::text, (timestamp with time zone :from))::bigint * 1000)
                and measured_to <= (date_part('epoch'::text, (timestamp with time zone :to))::bigint * 1000)
        ) mes on
			(date_part('epoch'::text, kalendar_1.a)::bigint * 1000) >= mes.measured_from
			and (date_part('epoch'::text, kalendar_1.a)::bigint * 1000) <= mes.measured_to
			and archive.tsk_std.id::text = mes.detector_id::text
		where
			archive.tsk_std.latitude is not null
			and archive.tsk_std.longitude is not null
	) select
		date_part('epoch'::text, kalendar.a::timestamp without time zone) * 1000::double precision as measured_at,
		ciselnik.geom,
		case
			when sum(
	            case
	                when sdata.measurement_type::text = 'count'::text and sdata.class_id = 1 and sdata.value is not null then 1
	                else 0
	            end) = 0 then null::bigint
			else sum(
	            case
	                when sdata.measurement_type::text = 'count'::text and sdata.class_id = 1 then sdata.value
	                else 0
	            end)
		end as sum,
		ciselnik.lokace as street,
		coalesce(jsonb_agg(json_build_object('detector_id', sdata.detector_id, 'measurement_type', sdata.measurement_type, 'class_id', sdata.class_id, 'class_type', cls.class_type, 'value', sdata.value)) filter (where sdata.value is not null), '[]'::jsonb) as related_details
	from
		ciselnik
	cross join kalendar
	left join sdata on
		ciselnik.latitude = sdata.latitude
		and ciselnik.longitude = sdata.longitude
		and kalendar.a = sdata.time_5min
		and sdata.value is not null
	left join archive.tsk_std_class_types cls on
		cls.id = sdata.class_id
	group by
		ciselnik.latitude,
		ciselnik.longitude,
		ciselnik.lokace,
		kalendar.a,
		ciselnik.geom
	order by
		ciselnik.lokace
) agg
