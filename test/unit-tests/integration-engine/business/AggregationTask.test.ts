import AggregationTask from "#ie/business/AggregationTask";
import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import { expect } from "chai";

describe("AggregationTask", () => {
    it("should map Date to unix timestamp in miliseconds", async () => {
        const newTask = new AggregationTask(
            new Date("2022-04-13T08:00:11Z"),
            new Date("2022-04-13T08:30:11Z"),
            AggregationTaskType.FCD
        );
        expect(newTask.fromUnixTimestamp).equals(1649836811000);
        expect(newTask.toUnixTimestamp).equals(1649838611000);
    });
    it("should map undefined Date to undefined", async () => {
        const newTask = {} as AggregationTask;
        expect(newTask.fromUnixTimestamp).equals(undefined);
        expect(newTask.toUnixTimestamp).equals(undefined);
    });
});
