import NdicEventsAggregator from "#ie/service/aggregators/NdicEventsAggregator";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { LineString, Point } from "geojson";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("NdicEventsAggregator", () => {
    let sandbox: SinonSandbox;
    let aggregator: NdicEventsAggregator;
    const mockupQueryData = [
        {
            situation_id: "6a3a95f3-5aae-4639-b6d0-905cadfc85fb",
            situation_record_type: "6a3a95f3-5aae-4639-b6d0-905cadfc85fb",
            situation_version_time: "2022-01-15 00:00:01.000 +0100",
            measured_at: "1643443200000",
            geom_origin: {
                //"LINESTRING (14.397614003554331 50.15981173046679, 14.397626911406451 50.15968580835093)",
                type: "LineString",
                coordinates: [
                    [14.397614003554331, 50.15981173046679],
                    [14.397626911406451, 50.15968580835093],
                ],
            } as LineString,
            geom_centroid: {
                // "POINT (14.397620457480391 50.159748769408864)"
                type: "Point",
                coordinates: [14.397620457480391, 50.159748769408864],
            } as Point,
            geom_startpoint: {
                //"POINT (14.397614003554331 50.15981173046679)",
                type: "Point",
                coordinates: [14.397614003554331, 50.15981173046679],
            } as Point,
            geom_endpoint: {
                //"POINT (14.397626911406451 50.15968580835093)",
                type: "Point",
                coordinates: [14.397626911406451, 50.15968580835093],
            },
            situation_urgency: "normalUrgency",
            geom_symbol_iconimage: "ndic_reconstruction",
            geom_line_pattern: "ndic_line_simple",
            properties: {
                gs: "2022-01-29T08:00:00+00:00",
                general_public_comment:
                    // eslint-disable-next-line max-len
                    "silnice II/242 (ulice Nádražní), Roztoky, okr. Praha-západ, Od 15.01.2022 00:00, Do 30.03.2022 23:59, částečná uzavírka silnice II/242, ul. Nádražní (před čp. 55), Roztoky, Vydal: Městský úřad Černošice",
                situation_version: "2",
                situation_id: "6a3a95f3-5aae-4639-b6d0-905cadfc85fb",
                situation_record_type: "ConstructionWorks",
                situation_record_creation_time: "2022-01-14T23:00:02+00:00",
                situation_version_time: "2022-01-14T23:00:01+00:00",
                situation_urgency: "normalUrgency",
                source: "SSU",
                geom_length_m: 14,
            },
        },
    ];
    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
                query: sandbox.stub().returns(Promise.resolve(mockupQueryData)),
            })
        );

        aggregator = new NdicEventsAggregator();
        sandbox.stub(aggregator["repository"], "saveData").callsFake(() => Promise.resolve());
        sandbox.stub(aggregator["repository"], <any>"createSemaphore").returns({
            tryAcquire: () => Promise.resolve(true),
            release: () => Promise.resolve(),
        });
        sandbox.spy(aggregator, <any>"aggregate");
        sandbox.spy(aggregator, <any>"save");
        sandbox.spy(aggregator["repository"], "validate");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return aggregated result", async () => {
        const result = await aggregator["aggregate"](new Date(), new Date());
        expect(result.length).equal(1);
        expect(typeof result[0].measured_at).equal("number");
    });

    it("should aggregate and save result", async () => {
        await aggregator.aggregateAndSave(new Date(), new Date(), false, true);
        sandbox.assert.calledOnce(aggregator["aggregate"] as SinonSpy);
        sandbox.assert.calledOnce(aggregator["save"] as SinonSpy);
        sandbox.assert.calledOnce(aggregator["repository"].validate as SinonSpy);
        sandbox.assert.calledOnce(aggregator["repository"].saveData as SinonSpy);
        sandbox.assert.callOrder(
            aggregator["aggregate"] as SinonSpy,
            aggregator["save"] as SinonSpy,
            aggregator["repository"].validate as SinonSpy,
            aggregator["repository"].saveData as SinonSpy
        );
    });

    it("should throw exception for invalid data", async () => {
        await expect(aggregator["repository"].validate(mockupQueryData)).to.be.rejectedWith(GeneralError);
    });
});
